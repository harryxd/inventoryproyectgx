create table if not exists articulos
(
    id_articulo varchar(1) default NULL::character varying not null
        constraint articulos_pkey
            primary key,
    articulo varchar(1) default NULL::character varying,
    descripcion varchar(1) default NULL::character varying,
    unidadmedida varchar(1) default NULL::character varying,
    grupo varchar(1) default NULL::character varying,
    marca varchar(1) default NULL::character varying,
    preciocompra varchar(1) default NULL::character varying,
    precioventa varchar(1) default NULL::character varying,
    fecharegistro varchar(1) default NULL::character varying,
    stock varchar(1) default NULL::character varying,
    articuloscol varchar(1) default NULL::character varying,
    serie varchar(255)
);

alter table articulos owner to comiuser;

create table if not exists usuarios
(
    id_usuario bigint not null
        constraint usuarios_pkey
            primary key,
    usuario_password text,
    nombre varchar(45) default NULL::character varying,
    eliminarventas boolean,
    generarreporte boolean,
    modificarventas boolean,
    realizarventas boolean
);

alter table usuarios owner to comiuser;

create table if not exists terceros
(
    id_tercero bigint not null
        constraint terceros_pkey
            primary key,
    "Direccion" varchar,
    "Telefono" varchar(9),
    "isProveedor" boolean,
    "RazonSocial" varchar,
    razon_social varchar(255) not null,
    is_proveedor boolean not null
);

alter table terceros owner to comiuser;

create table if not exists entradas
(
    "Id_Entrada" integer not null
        constraint entradas_pkey
            primary key,
    "FechaEntrada" date,
    "FechaFactura" date,
    "Total" double precision,
    "Moneda" "char",
    fecha_entrada timestamp not null,
    fecha_factura timestamp not null,
    tercero_id bigint
        constraint fkiqffy4f847x32irw19mtf7rnc
            references terceros,
    usuario_id bigint
        constraint fkl8pejffws2pfeud1lo50g55hv
            references usuarios
);

alter table entradas owner to comiuser;

create table if not exists entradas_detalle
(
    "Cantidad" integer,
    entradas_id integer not null
        constraint fkn82w171c83btnobs4nd15csnc
            references entradas,
    id_entradas_detalle integer not null,
    articulo_id varchar(255) not null
        constraint fks0v9u3uy9bg20ln8evt25pnfq
            references articulos,
    precio_compra_unitario double precision,
    constraint entradas_detalle_pk
        primary key (id_entradas_detalle, entradas_id)
);

alter table entradas_detalle owner to comiuser;

create table if not exists ventas
(
    "Id_Venta" integer not null
        constraint ventas_pkey
            primary key,
    "FechaFactura" date,
    "Total" double precision,
    "Moneda" varchar,
    fecha_factura date,
    tercero_id bigint
        constraint fkr9psmb6d5aipmh396bwrjtqp9
            references terceros,
    usuario_id bigint
        constraint fkco9r9xjcdqtgd4nvnnolsr6ei
            references usuarios
);

alter table ventas owner to comiuser;

create table if not exists ventas_detalle
(
    "Id_Articulo" varchar,
    "Cantidad" integer,
    id_ventas_detalle integer not null,
    ventas_id integer not null
        constraint fks9tjps1ng67hkxonu7n8vwlu5
            references ventas,
    descuento_unitario double precision,
    precio_venta_unitario double precision,
    fk_articulo varchar(255) not null
        constraint fkd0riqwxqt0gw7r7fs62yftkef
            references articulos,
    constraint ventas_detalle_pk
        primary key (id_ventas_detalle, ventas_id)
);

alter table ventas_detalle owner to comiuser;

