package pe.edu.unsa.gabrielalex.spring.entity;

import javax.persistence.*;

@Entity
@Table(name = "entradas_detalle")
public class Entrada_Detalle {
    @EmbeddedId
    private PK_ED claves;
    @ManyToOne
    @JoinColumn(name = "ENTRADAS_ID",insertable=false, updatable=false)
    private entradas entradas;

    @ManyToOne
    @JoinColumn(name = "ARTICULO_ID", updatable = false, nullable = false)
    private Articulos articulo;
    @Column(name="Cantidad")
    private int cantidad;
    @Column(name="PrecioCompraUnitario")
    private double precioUnitario;
}
