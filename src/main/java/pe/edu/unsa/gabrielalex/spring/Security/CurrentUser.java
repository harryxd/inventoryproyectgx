package pe.edu.unsa.gabrielalex.spring.Security;

import pe.edu.unsa.gabrielalex.spring.entity.Usuario;

@FunctionalInterface
public interface CurrentUser {
    Usuario getUsuario();
}
