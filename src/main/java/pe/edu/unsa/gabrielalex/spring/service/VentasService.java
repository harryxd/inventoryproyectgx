package pe.edu.unsa.gabrielalex.spring.service;

import org.springframework.stereotype.Service;
import pe.edu.unsa.gabrielalex.spring.entity.Ventas;
import pe.edu.unsa.gabrielalex.spring.repository.VentasRepository;

import java.util.List;

@Service
public class VentasService {
    private final VentasRepository vr;

    public VentasService(VentasRepository vr) {
        this.vr = vr;
    }
//    public List<VentasRepository>listOrder(){
//
//    }

    public List<Ventas> all() {

        return vr.findAll();
    }
}
