package pe.edu.unsa.gabrielalex.spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import pe.edu.unsa.gabrielalex.spring.entity.terceros;

public interface TercerosRepository extends JpaRepository<terceros, Long> {

}