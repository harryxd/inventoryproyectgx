package pe.edu.unsa.gabrielalex.spring.entity;

import javax.persistence.*;

@Entity
@Table(name = "ventas_detalle")
public class Ventas_Detalle {
    @EmbeddedId
    private PK_VD claves;
    @ManyToOne
    @JoinColumn(name = "VENTAS_ID",insertable=false, updatable=false)
    private Ventas ventas;

    @OneToOne
    @JoinColumn(name = "FK_ARTICULO", updatable = false, nullable = false)
    private Articulos articulo;

    @Column(name="Cantidad")
    private int cantidad;
    @Column(name="PrecioVentaUnitario")
    private double precioventaunitario;
    @Column(name="DescuentoUnitario")
    private double descuentounitario;

    public Ventas_Detalle(int cantidad, double precioventaunitario, double descuentounitario) {
        this.cantidad = cantidad;
        this.precioventaunitario = precioventaunitario;
        this.descuentounitario = descuentounitario;
    }

    public PK_VD getClaves() {
        return claves;
    }

    public void setClaves(PK_VD claves) {
        this.claves = claves;
    }

    public Ventas getVentas() {
        return ventas;
    }

    public void setVentas(Ventas ventas) {
        this.ventas = ventas;
    }

    public Articulos getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulos articulo) {
        this.articulo = articulo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecioventaunitario() {
        return precioventaunitario;
    }

    public void setPrecioventaunitario(double precioventaunitario) {
        this.precioventaunitario = precioventaunitario;
    }

    public double getDescuentounitario() {
        return descuentounitario;
    }

    public void setDescuentounitario(double descuentounitario) {
        this.descuentounitario = descuentounitario;
    }
}
