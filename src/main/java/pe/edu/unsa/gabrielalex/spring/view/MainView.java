package pe.edu.unsa.gabrielalex.spring.view;

import com.github.appreciated.app.layout.addons.notification.DefaultNotificationHolder;
import com.github.appreciated.app.layout.addons.notification.component.NotificationButton;
import com.github.appreciated.app.layout.component.appbar.AppBarBuilder;
import com.github.appreciated.app.layout.component.applayout.LeftLayouts;
import com.github.appreciated.app.layout.component.builder.AppLayoutBuilder;
import com.github.appreciated.app.layout.component.menu.left.builder.LeftAppMenuBuilder;
import com.github.appreciated.app.layout.component.menu.left.builder.LeftSubMenuBuilder;
import com.github.appreciated.app.layout.component.menu.left.items.LeftClickableItem;
import com.github.appreciated.app.layout.component.menu.left.items.LeftNavigationItem;
import com.github.appreciated.app.layout.component.menu.left.items.LeftSectionItem;
import com.github.appreciated.app.layout.component.router.AppLayoutRouterLayout;
import com.github.appreciated.app.layout.entity.DefaultBadgeHolder;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.spring.annotation.UIScope;

import com.vaadin.flow.server.PWA;
import org.springframework.stereotype.Component;

import static com.github.appreciated.app.layout.entity.Section.HEADER;
import static com.github.appreciated.app.layout.entity.Section.FOOTER;

@Push
@Viewport("width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=yes")
@PWA(name = "Project Base for Vaadin Flow with Spring", shortName = "Project Base")
/*
@Theme(value = Lumo.class, variant = Lumo.DARK )
*/
@Component
@UIScope
public class MainView extends AppLayoutRouterLayout<LeftLayouts.LeftResponsive> {
    private DefaultNotificationHolder notifications = new DefaultNotificationHolder();
    private DefaultBadgeHolder badge = new DefaultBadgeHolder(2);
    private String title = "MENU";
    private Button user_session;
    private LeftNavigationItem arti;

    public MainView() {
        notifications.addClickListener(notifications -> {/*  */});
        LeftNavigationItem menu = new LeftNavigationItem(title, VaadinIcon.MENU.create(), View5.class);
        badge.bind(menu.getBadge());
        user_session = new Button("User");
         arti = new LeftNavigationItem("Articulos", VaadinIcon.BOOK.create(), GridTest.class);
        init(AppLayoutBuilder.get(LeftLayouts.LeftResponsive.class)
                .withAppBar(AppBarBuilder.get()
                        .add(new Image("img/logo.png", "logo"))
                        .build()).withTitle(title)
                .withAppBar(AppBarBuilder.get()
                        .add(user_session)
                        .add(new NotificationButton<>(VaadinIcon.BELL, notifications))
                        .build())
                .withAppMenu(LeftAppMenuBuilder.get()
                        .addToSection(HEADER,
                                new LeftSectionItem("COMISOFTWARE")//,
                                //new LeftClickableItem("Clickable Entry", VaadinIcon.COG.create(), clickEvent -> Notification.show("onClick ..."))
                        )
                        .add(new LeftNavigationItem("Home", VaadinIcon.HOME.create(), MenuView.class),
                                arti,
                                new LeftNavigationItem("Reportes", VaadinIcon.CHART_LINE.create(), GridTest.class),
                                new LeftNavigationItem("Compras", VaadinIcon.CART.create(), Compras.class),
                                new LeftNavigationItem("Almacen", VaadinIcon.ARCHIVE.create(), CrudArticulo.class),
                                new LeftNavigationItem("Venta", VaadinIcon.DOLLAR.create(), Ventas.class),
                                new LeftNavigationItem("Manual", VaadinIcon.OPEN_BOOK.create(), GridTest.class),
                                new LeftNavigationItem("Usuarios", VaadinIcon.USERS.create(), CrudUsuario.class),
                                LeftSubMenuBuilder.get("Venta", VaadinIcon.PLUS.create())
                                        .add(new LeftNavigationItem("CRUD", VaadinIcon.SPLINE_CHART.create(), View2.class),
                                                new LeftNavigationItem("Salida", VaadinIcon.CONNECT.create(), View3.class),
                                                new LeftNavigationItem("More", VaadinIcon.COG.create(), View4.class))
                                        .build(), menu)
                        .addToSection(FOOTER, new LeftClickableItem("Clickable Entry", VaadinIcon.COG.create(), clickEvent -> Notification.show("onClick ...")))
                        .build())
                .build());
        user_session.addClickListener(buttonClickEvent -> {
            VaadinSession.getCurrent().getSession().invalidate();
            UI.getCurrent().getPage().executeJavaScript("window.location.href='login'");
        });
    }

    public DefaultNotificationHolder getNotifications() {
        return notifications;
    }

    public DefaultBadgeHolder getBadge() {
        return badge;
    }
    public Button getUser_session() {
        return user_session;
    }

    public void setUser_session(String name) {
        this.user_session.setText(name);
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LeftNavigationItem getArti() {
        return arti;
    }

    public void setArti(LeftNavigationItem arti) {
        this.arti = arti;
    }
}
