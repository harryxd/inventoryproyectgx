package pe.edu.unsa.gabrielalex.spring.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import pe.edu.unsa.gabrielalex.spring.entity.Usuario;

import java.util.List;

public interface UsuarioRepository extends JpaRepository<Usuario,Long> {

    Usuario findByNombre(String username);

    List<Usuario> findByNombreContainingIgnoreCase(String nombre);
    Page<Usuario> findBy(Pageable pageable);

}
