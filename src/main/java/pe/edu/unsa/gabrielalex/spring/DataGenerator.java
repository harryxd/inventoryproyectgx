package pe.edu.unsa.gabrielalex.spring;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.vaadin.flow.spring.annotation.SpringComponent;
import pe.edu.unsa.gabrielalex.spring.entity.Usuario;
import pe.edu.unsa.gabrielalex.spring.repository.UsuarioRepository;

@SpringComponent
public class DataGenerator implements HasLogger {

    private UsuarioRepository usuarioRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public DataGenerator(UsuarioRepository usuarioRepository,PasswordEncoder passwordEncoder) {
        this.usuarioRepository = usuarioRepository;
        this.passwordEncoder= passwordEncoder;
    }
    @PostConstruct
    public void loadData(){

        getLogger().info("Generating demo data");
        getLogger().info("... generating users");
        //Usuario user=createUsuario(usuarioRepository,passwordEncoder);
        getLogger().info("Generated demo data");
    }

    private Usuario createUsuario(UsuarioRepository usuarioRepository, PasswordEncoder passwordEncoder) {
        return usuarioRepository.save(createUser("pedro",passwordEncoder.encode("jperez")));

    }

    private Usuario createUser(String juan_perez, String jperez) {
        Usuario user= new Usuario();
        user.setEliminarventas(true);
        user.setGenerarreporte(true);
        user.setModificarventas(true);
        user.setNombre(juan_perez);
        user.setRealizarventas(true);
        user.setRole("admin");
        user.setUsuario_password(jperez);
        return user;
    }
}