package pe.edu.unsa.gabrielalex.spring.service;

import org.springframework.stereotype.Service;
import pe.edu.unsa.gabrielalex.spring.entity.Articulos;
import pe.edu.unsa.gabrielalex.spring.repository.ArticulosRepository;

@Service
public class ArticuloService {
private final ArticulosRepository ar;

    public ArticuloService(ArticulosRepository ar) {
        this.ar = ar;
    }
    public Articulos save(Articulos articulos){
        return ar.save(articulos);
    }
}
