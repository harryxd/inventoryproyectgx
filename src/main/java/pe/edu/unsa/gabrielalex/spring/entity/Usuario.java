package pe.edu.unsa.gabrielalex.spring.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name="usuarios")
public class Usuario {
    @NotNull
    @Id
    @GeneratedValue
    @Column(name = "id_usuario")
    private Long idU;
    @NotNull
    @Column(name = "usuario_password")
    private String usuario_password;
    @NotNull
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "realizarventas")
    private Boolean realizarventas;
    @Column(name = "modificarventas")
    private Boolean modificarventas;
    @Column(name = "eliminarventas")
    private Boolean eliminarventas;
    @Column(name = "generarreporte")
    private Boolean generarreporte;
    @Column(name = "role")
    private String role;

    @OneToMany(targetEntity = entradas.class, mappedBy = "usuario" )
    private List<entradas> entradasList;
    @OneToMany(targetEntity = Ventas.class, mappedBy = "usuario" )
    private List<Ventas> ventasList;
    public Usuario() {
    }

    public Usuario(@NotNull String usuario_password, @NotNull String nombre, Boolean realizarventas, Boolean modificarventas, Boolean eliminarventas, Boolean generarreporte, String role) {
        this.usuario_password = usuario_password;
        this.nombre = nombre;
        this.realizarventas = realizarventas;
        this.modificarventas = modificarventas;
        this.eliminarventas = eliminarventas;
        this.generarreporte = generarreporte;
        this.role = role;
    }

    public Long getIdU() {
        return idU;
    }

    public void setIdU(Long idU) {
        this.idU = idU;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Long getId_usuario() {
        return idU;
    }

    public void setId_usuario(Long id_usuario) {
        this.idU = id_usuario;
    }

    public String getUsuario_password() {
        return usuario_password;
    }

    public void setUsuario_password(String usuario_password) {
        this.usuario_password = usuario_password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getRealizarventas() {
        return realizarventas;
    }

    public void setRealizarventas(Boolean realizarventas) {
        this.realizarventas = realizarventas;
    }

    public Boolean getModificarventas() {
        return modificarventas;
    }

    public void setModificarventas(Boolean modificarventas) {
        this.modificarventas = modificarventas;
    }

    public Boolean getEliminarventas() {
        return eliminarventas;
    }

    public void setEliminarventas(Boolean eliminarventas) {
        this.eliminarventas = eliminarventas;
    }

    public Boolean getGenerarreporte() {
        return generarreporte;
    }

    public void setGenerarreporte(Boolean generarreporte) {
        this.generarreporte = generarreporte;
    }
}
