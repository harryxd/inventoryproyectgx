package pe.edu.unsa.gabrielalex.spring.entity;

import javax.persistence.*;
import java.util.Calendar;
import java.util.List;

@Entity
@Table
public class Ventas {
    @Id
    @GeneratedValue
    @Column(name = "Id_Venta")
    private int Id_Venta;
    @Column(name = "FechaFactura")
    @Temporal(TemporalType.DATE)
    private Calendar FechaFactura;
    @Column(name = "Total")
    private double Total;
    @Column(name = "Moneda")
    private String Moneda;
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "TERCERO_ID")
    private terceros tercero;
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "USUARIO_ID")
    private Usuario usuario;
    @OneToMany(mappedBy = "ventas")
    private List<Ventas_Detalle> ventasList;


    public Ventas(Calendar fechaFactura, double total, String moneda) {
        FechaFactura = fechaFactura;
        Total = total;
        Moneda = moneda;
    }

    public int getId_Venta() {
        return Id_Venta;
    }

    public void setId_Venta(int id_Venta) {
        Id_Venta = id_Venta;
    }

    public Calendar getFechaFactura() {
        return FechaFactura;
    }

    public void setFechaFactura(Calendar fechaFactura) {
        FechaFactura = fechaFactura;
    }

    public double getTotal() {
        return Total;
    }

    public void setTotal(double total) {
        Total = total;
    }

    public String getMoneda() {
        return Moneda;
    }

    public void setMoneda(String moneda) {
        Moneda = moneda;
    }

    public terceros getTercero() {
        return tercero;
    }

    public void setTercero(terceros tercero) {
        this.tercero = tercero;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
