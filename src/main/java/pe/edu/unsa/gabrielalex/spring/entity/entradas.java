package pe.edu.unsa.gabrielalex.spring.entity;


import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="entradas")
public class entradas {
    @Id
    @NotNull
    @GeneratedValue
    @Column(name ="Id_Entrada")
    private int Id_Entrada;


    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "TERCERO_ID")
    private terceros tercero;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "USUARIO_ID")
    private Usuario usuario;
    @OneToMany(mappedBy = "entradas")
    private List<Entrada_Detalle> entradasList;
    @NotNull
    @Column(name ="FechaEntrada")
    @Temporal(TemporalType.DATE)
    private Date FechaEntrada;

    @NotNull
    @Column(name ="FechaFactura")
    @Temporal(TemporalType.DATE)

    private Date FechaFactura;

    @NotNull
    @Column(name ="Total")
    private double Total;

    @NotNull
    @Column(name ="Moneda")
    private char Moneda;

    public entradas() {
    }

    public entradas(@NotNull Date fechaEntrada, @NotNull Date fechaFactura, @NotNull double total, @NotNull char moneda) {
        FechaEntrada = fechaEntrada;
        FechaFactura = fechaFactura;
        Total = total;
        Moneda = moneda;
    }

    public int getId_Entrada() {
        return Id_Entrada;
    }

    public void setId_Entrada(int id_Entrada) {
        Id_Entrada = id_Entrada;
    }

    public terceros getId_Tercero() {
        return tercero;
    }

    public void setId_Tercero(terceros id_Tercero) {
        tercero = id_Tercero;
    }


    public Usuario getId_Usuario() {
        return usuario;
    }

    public void setId_Usuario(Usuario id_Usuario) {
        usuario = id_Usuario;
    }

    public Date getFechaEntrada() {
        return FechaEntrada;
    }

    public void setFechaEntrada(Date fechaEntrada) {
        FechaEntrada = fechaEntrada;
    }

    public Date getFechaFactura() {
        return FechaFactura;
    }

    public void setFechaFactura(Date fechaFactura) {
        FechaFactura = fechaFactura;
    }

    public double getTotal() {
        return Total;
    }

    public void setTotal(double total) {
        Total = total;
    }

    public char getMoneda() {
        return Moneda;
    }

    public void setMoneda(char moneda) {
        Moneda = moneda;

    }

}
