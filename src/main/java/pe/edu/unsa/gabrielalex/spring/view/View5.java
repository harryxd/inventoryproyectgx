package pe.edu.unsa.gabrielalex.spring.view;


import com.vaadin.flow.router.Route;

@Route(value = "view5", layout = MainView.class)
public class View5 extends AbstractView {
    @Override
    String getViewName() {
        return getClass().getName();
    }
}