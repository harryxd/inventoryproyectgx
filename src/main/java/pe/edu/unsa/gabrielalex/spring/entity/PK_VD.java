package pe.edu.unsa.gabrielalex.spring.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class PK_VD implements Serializable {
        public int id_ventas_detalle;
        private int ventas_id;


    public int getId_Ventas_detalle() {
        return id_ventas_detalle;
    }

    public void setId_Ventas_detalle(int id_Ventas_detalle) {
        id_ventas_detalle = id_Ventas_detalle;
    }

    public int getVentas_id() {
        return ventas_id;
    }

    public void setVentas_id(int ventas_id) {
        this.ventas_id = ventas_id;
    }

    public PK_VD() {
    }

    public PK_VD(int id_Ventas_detalle, int ventas_id) {
        id_ventas_detalle = id_Ventas_detalle;
        this.ventas_id = ventas_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PK_VD)) return false;
        PK_VD pk_vd = (PK_VD) o;
        return getId_Ventas_detalle() == pk_vd.getId_Ventas_detalle() &&
                getVentas_id() == pk_vd.getVentas_id();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(getId_Ventas_detalle());
        hash = 59 * hash + Objects.hashCode(getVentas_id());
        return hash;
    }
}
