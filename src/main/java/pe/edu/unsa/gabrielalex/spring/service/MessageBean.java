package pe.edu.unsa.gabrielalex.spring.service;

import org.springframework.stereotype.Service;

import java.time.LocalTime;

@Service
public class MessageBean {
    public static final int USER_COUNT_LIMIT =1000;
    public String getMessage() {
        return "Button was clicked at " + LocalTime.now();
    }
    /*public static class LimitReached extends RuntimeException{

    }
    private final UsuarioRepository repo;

    public UsuarioService(UsuarioRepository repo) {
        this.repo = repo;
    }
    public List<Usuario> findAll(){
        return repo.findAll();
    }
    public int countAll(){
        return (int)repo.count();
    }
    public List<Usuario> findByNombreContainingIgnoreCase(String nombre){
        return repo.findByNombreContainingIgnoreCase(nombre);
    }
    public Usuario save(Usuario user){
        if(countAll()>=USER_COUNT_LIMIT){
            throw new LimitReached();
        }
        return repo.save(user);
    }
    public void delete(Usuario user){
        repo.delete(user);
    }
*/

}
