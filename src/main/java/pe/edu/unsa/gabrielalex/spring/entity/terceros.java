package pe.edu.unsa.gabrielalex.spring.entity;

import net.bytebuddy.build.ToStringPlugin;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name="terceros")
public class terceros {
    @Id
    @NotNull
    @GeneratedValue
    @Column(name = "id_tercero")
    private Long id_tercero;
    @NotNull
    @Column(name = "Direccion")
    private String Direccion;
    @NotNull
    @Column(name = "Telefono")
    private String Telefono;
    @NotNull
    @Column(name = "is_proveedor")
    private boolean isProveedor;
    @NotNull
    @Column(name = "razon_social")
    private String RazonSocial;


    @OneToMany( mappedBy = "tercero", cascade = CascadeType.ALL)
    private List<entradas> entrada;
    @OneToMany(targetEntity = Ventas.class, mappedBy = "usuario", cascade=CascadeType.ALL )
    private List<Ventas> ventasList;

    public terceros(@NotNull String direccion, @NotNull String telefono, @NotNull boolean isProveedor, @NotNull String razonSocial) {
        Direccion = direccion;
        Telefono = telefono;
        this.isProveedor = isProveedor;
        RazonSocial = razonSocial;
    }

    public Long getId_tercero() {
        return id_tercero;
    }

    public void setId_tercero(Long id_tercero) {
        this.id_tercero = id_tercero;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String direccion) {
        Direccion = direccion;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }

    public boolean isProveedor() {
        return isProveedor;
    }

    public void setProveedor(boolean proveedor) {
        isProveedor = proveedor;
    }

    public String getRazonSocial() {
        return RazonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        RazonSocial = razonSocial;
    }
}
