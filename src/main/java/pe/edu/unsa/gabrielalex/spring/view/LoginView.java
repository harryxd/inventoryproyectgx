package pe.edu.unsa.gabrielalex.spring.view;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.login.LoginI18n;
import com.vaadin.flow.component.login.LoginOverlay;

import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.router.*;
import pe.edu.unsa.gabrielalex.spring.Security.SecurityUtils;


@Route(value="login")
//@PageTitle("vaadin-demo-bakery-app")
@JsModule("./styles/shared-styles.js")
@Viewport("width=device-width, minimum-scale=1, initial-scale=1, user-scalable=yes, viewport-fit=cover")
public class LoginView extends LoginOverlay
        implements AfterNavigationObserver, BeforeEnterObserver {

    public LoginView() {
        LoginI18n i18n = LoginI18n.createDefault();
        i18n.setHeader(new LoginI18n.Header());
        i18n.getHeader().setTitle("SUBACOM");
        i18n.getHeader().setDescription("comiSOFTWARE");
        i18n.setAdditionalInformation(null);
        i18n.setForm(new LoginI18n.Form());
        i18n.getForm().setSubmit("Sign in");
        i18n.getForm().setTitle("Sign in");
        i18n.getForm().setUsername("Name");
        i18n.getForm().setPassword("Password");
        setI18n(i18n);
        setForgotPasswordButtonVisible(false);
        setAction("login");
        getElement().setVisible(true);

    }

    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        if (SecurityUtils.isUserLoggedIn()) {
            UI.getCurrent().getPage().executeJavaScript("window.location.href='index'");
        } else {
            setOpened(true);
        }
    }

    @Override
    public void afterNavigation(AfterNavigationEvent  event) {
        setError(
                event.getLocation().getQueryParameters().getParameters().containsKey(
                        "error"));

    }
}