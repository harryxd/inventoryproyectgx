package pe.edu.unsa.gabrielalex.spring.view;

import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.H6;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;
import org.vaadin.crudui.crud.CrudOperation;
import org.vaadin.crudui.crud.impl.GridCrud;
import pe.edu.unsa.gabrielalex.spring.entity.Articulos;
import pe.edu.unsa.gabrielalex.spring.entity.Usuario;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@Route(value = "GridTest", layout = MainView.class)
public class CrudArticulo extends VerticalLayout {
    private List<GridTest.Person> people = new ArrayList<>();

    public CrudArticulo() {
        GridCrud<Articulos> crud = new GridCrud<>(Articulos.class);
        TextField filtro =new TextField();
        filtro.setPlaceholder("Filtro por nombre");
        filtro.setClearButtonVisible(true);
        crud.getCrudLayout().addFilterComponent(filtro);

        crud.getGrid().setColumns("id_articulo","articulo","descripcion","unidadmedida","grupo","marca","modelo","preciocompra","precioventa","fecharegistro","stock");
        crud.getGrid().setColumnReorderingAllowed(true);
        crud.getCrudFormFactory().setUseBeanValidation(true);
        crud.getCrudFormFactory().setVisibleProperties(
                "nombre","modificarventas","realizarventas","eliminarventas","generarreporte"
        );
        crud.getCrudFormFactory().setVisibleProperties(
                CrudOperation.UPDATE,
                "nombre","usuario_password","modificarventas","realizarventas","eliminarventas","generarreporte");
        crud.getCrudFormFactory().setVisibleProperties(
                CrudOperation.ADD,
                "nombre","usuario_password","modificarventas","realizarventas","eliminarventas","generarreporte"
        );
        crud.getCrudFormFactory().setFieldType("usuario_password", PasswordField.class);

        setSizeFull();
        add(crud);
        crud.setOperations(
                () -> usuarioService.findByNombreContainingIgnoreCase(filtro.getValue()),
                user -> usuarioService.save(user),
                user -> usuarioService.save(user),
                user -> usuarioService.delete(user)
        );
        crud.getGrid().addItemDoubleClickListener(event-> {
            usuarioView(event.getItem());
        });
    }

    private void usuarioView(Usuario item) {
        HorizontalLayout hl = new HorizontalLayout();
        HorizontalLayout h2 = new HorizontalLayout();
        H3 nombre= new H3("Nombre: "+item.getNombre());
        H3 id = new H3("ID: "+String.valueOf(item.getId_usuario()));
        id.getStyle().set("color", "gray");
        id.getStyle().set("text-align","right");
        Dialog swd = new Dialog();
        swd.add(id);
        swd.add(nombre);
        Hr pan = new Hr();
        swd.add(new H6("Acciones que puede realizar:"));
        swd.add(pan);
        swd.add(new H6("usted "+puede(item.getEliminarventas())+" puede eliminar ventas"));
        swd.add(new H6("usted "+puede(item.getGenerarreporte())+" puede generar reporte"));
        swd.add(new H6("usted "+puede(item.getModificarventas())+" puede modificar ventas"));
        swd.add(new H6("usted "+puede(item.getRealizarventas())+" puede realizar ventas"));
        swd.setWidth("800px");
        swd.setHeight("550px");
        swd.open();
    }
    private String puede(boolean b){
        if(b){
            return "si";
        }
        return "no";
    }
}