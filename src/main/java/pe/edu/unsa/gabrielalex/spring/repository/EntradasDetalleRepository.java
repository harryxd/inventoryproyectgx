package pe.edu.unsa.gabrielalex.spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import pe.edu.unsa.gabrielalex.spring.entity.Entrada_Detalle;

public interface EntradasDetalleRepository extends JpaRepository<Entrada_Detalle, Integer>{
}