package pe.edu.unsa.gabrielalex.spring.view;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;

@Route(value = "compras", layout = MainView.class)
public class Compras extends VerticalLayout {
    public Compras() {

        H1 subacom= new H1("SUBACOM E.I.R.L");
        subacom.setWidth(null);
        ComboBox chekTipoProd = new ComboBox();
        chekTipoProd.setLabel("Tipo de Producto");
        TextField marca = new TextField();
        marca.setLabel("Marca");
        TextField modelo = new TextField();
        modelo.setLabel("Modelo");
        TextField stock = new TextField();
        stock.setLabel("Stock");
        TextField color = new TextField();
        color.setLabel("Color");
        Button guardar = new Button();
        guardar.setText("Guardar");
        guardar.getStyle().set("danger" ,"danger ");

        HorizontalLayout marcaModelo = new HorizontalLayout();
        marcaModelo.add(marca,modelo);
        HorizontalLayout stockColor = new HorizontalLayout();
        stockColor.add(stock,color);
        add(subacom,chekTipoProd, marcaModelo);
        Label sd= new Label();
        add(sd,stockColor);
        add(guardar);
        setAlignItems(Alignment.CENTER);
    }
}
