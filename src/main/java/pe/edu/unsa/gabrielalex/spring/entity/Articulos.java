package pe.edu.unsa.gabrielalex.spring.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="articulos")
public class Articulos {
    @Id
    @NotNull
    @GeneratedValue
    @Column(name="id_articulo")
    private String id_articulo;
    @Column(name="articulo")
    private String articulo;
    @Column(name="descripcion")
    private String descripcion;
    @Column(name="unidadmedida")
    private String unidadmedida;
    @Column(name="grupo")
    private String grupo;
    @Column(name="marca")
    private String marca;
    @Column(name = "modelo")
    private String modelo;
    @Column(name="preciocompra")
    private Double preciocompra;
    @Column(name="precioventa")
    private Double precioventa;

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    @Column(name="fecharegistro")
    private Date fecharegistro;
    @Column(name="stock")
    private int stock;
    @OneToMany(mappedBy = "articulo", cascade = CascadeType.ALL)
    private List<Ventas_Detalle> ventasList;
    public Articulos(String articulo, String descripcion, String unidadmedida, String grupo, String marca, Double preciocompra, Double precioventa, Date fecharegistro, int stock) {
        this.articulo = articulo;
        this.descripcion = descripcion;
        this.unidadmedida = unidadmedida;
        this.grupo = grupo;
        this.marca = marca;
        this.preciocompra = preciocompra;
        this.precioventa = precioventa;
        this.fecharegistro = fecharegistro;
        this.stock = stock;
    }

    public Articulos() {
    }

    public String getId_articulo() {
        return id_articulo;
    }

    public void setId_articulo(String id_articulo) {
        this.id_articulo = id_articulo;
    }

    public String getArticulo() {
        return articulo;
    }

    public void setArticulo(String articulo) {
        this.articulo = articulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUnidadmedida() {
        return unidadmedida;
    }

    public void setUnidadmedida(String unidadmedida) {
        this.unidadmedida = unidadmedida;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public Double getPreciocompra() {
        return preciocompra;
    }

    public void setPreciocompra(Double preciocompra) {
        this.preciocompra = preciocompra;
    }

    public Double getPrecioventa() {
        return precioventa;
    }

    public void setPrecioventa(Double precioventa) {
        this.precioventa = precioventa;
    }

    public Date getFecharegistro() {
        return fecharegistro;
    }

    public void setFecharegistro(Date fecharegistro) {
        this.fecharegistro = fecharegistro;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
}
