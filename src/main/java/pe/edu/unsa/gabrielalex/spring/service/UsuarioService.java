package pe.edu.unsa.gabrielalex.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pe.edu.unsa.gabrielalex.spring.entity.Usuario;
import pe.edu.unsa.gabrielalex.spring.repository.UsuarioRepository;

import java.time.LocalTime;
import java.util.List;

@Service
public class UsuarioService {
    public static final int USER_COUNT_LIMIT =1000;
    public String getMessage() {
        return "Button was clicked at " + LocalTime.now();
    }
    public static class LimitReached extends RuntimeException{

    }
    private final UsuarioRepository repo;
    @Autowired
    private static final PasswordEncoder BCRYPT = new BCryptPasswordEncoder();
    private PasswordEncoder passwordEncoder = new PasswordEncoder() {
        @Override
        public String encode(CharSequence charSequence) {
            return BCRYPT.encode(charSequence);
        }

        @Override
        public boolean matches(CharSequence charSequence, String s) {
            return BCRYPT.matches(charSequence, s);        }
    };
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    public UsuarioService(UsuarioRepository repo) {
        this.repo = repo;
    }
    public List<Usuario> findAll(){
        return repo.findAll();
    }
    public int countAll(){
        return (int)repo.count();
    }
    public List<Usuario> findByNombreContainingIgnoreCase(String nombre){
        return repo.findByNombreContainingIgnoreCase(nombre);
    }
    public Usuario save(Usuario user){
        if(countAll()>=USER_COUNT_LIMIT){
            throw new LimitReached();
        }


        user.setUsuario_password(passwordEncoder.encode(user.getUsuario_password()));
        return repo.save(user);
    }
    public void delete(Usuario user){
        repo.delete(user);
    }


}