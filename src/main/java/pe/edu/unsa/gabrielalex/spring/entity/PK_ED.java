package pe.edu.unsa.gabrielalex.spring.entity;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class PK_ED implements Serializable {
    public int id_entradas_detalle;
    private int entradas_id;

    public PK_ED(int id_entradas_detalle, int entradas_id) {
        this.id_entradas_detalle = id_entradas_detalle;
        this.entradas_id = entradas_id;
    }

    public int getId_entradas_detalle() {
        return id_entradas_detalle;
    }

    public void setId_entradas_detalle(int id_entradas_detalle) {
        this.id_entradas_detalle = id_entradas_detalle;
    }

    public int getEntradas_id() {
        return entradas_id;
    }

    public void setEntradas_id(int entradas_id) {
        this.entradas_id = entradas_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PK_ED)) return false;
        PK_ED pk_ed = (PK_ED) o;
        return getId_entradas_detalle() == pk_ed.getId_entradas_detalle() &&
                getEntradas_id() == pk_ed.getEntradas_id();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId_entradas_detalle(), getEntradas_id());
    }
}
