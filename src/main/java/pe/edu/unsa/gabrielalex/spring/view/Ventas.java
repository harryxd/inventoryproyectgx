package pe.edu.unsa.gabrielalex.spring.view;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;

@Route(value = "ventas", layout = MainView.class)
public class Ventas extends VerticalLayout {
    public Ventas() {
        H1 subacom= new H1("SUBACOM E.I.R.L");
        subacom.setWidth("900px");
        ComboBox comboProducto = new ComboBox();
        ComboBox comboMarca = new ComboBox();
        ComboBox comboModelo = new ComboBox();
        ComboBox comboColor = new ComboBox();
        comboProducto.setLabel("Producto");
        comboMarca.setLabel("Marca");
        comboModelo.setLabel("Modelo");
        comboColor.setLabel("Color");
        Button guardar = new Button();
        guardar.setText("Confirmar");
        HorizontalLayout footerLayout = new HorizontalLayout();

        footerLayout.add(guardar);
        footerLayout.setAlignItems(Alignment.END);
        guardar.getStyle().set("danger" ,"danger ");

        HorizontalLayout marcaModelo = new HorizontalLayout();
        marcaModelo.add(comboProducto,comboMarca);
        HorizontalLayout stockColor = new HorizontalLayout();
        stockColor.add(comboModelo,comboColor);
        add(subacom,marcaModelo);
        Label sd= new Label();
        add(sd,stockColor);
        add(footerLayout);
        setAlignItems(Alignment.CENTER);
    }
}
