package pe.edu.unsa.gabrielalex.spring.view;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinRequest;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.VaadinSession;
import org.apache.catalina.webresources.FileResource;
import org.apache.tomcat.jni.File;
import org.springframework.beans.factory.annotation.Autowired;
import pe.edu.unsa.gabrielalex.spring.service.MessageBean;

@Route(value = "index", layout = MainView.class)
public class MenuView extends VerticalLayout {

    public MenuView(@Autowired MessageBean bean, @Autowired MainView appLayout) {

        H1 subacom = new H1("SUBACOM E.I.R.L");
        subacom.setWidth(null);
        add(subacom);
        Html img = new Html("<img src='C:\\Users\\harry\\IdeaProjects\\inventoryproyectgx\\src\\main\\webapp\\img\\articulos.png'>");
        add(img);

        setAlignItems(Alignment.CENTER);
        add(getLabel());

        appLayout.setUser_session("Pedro");
        appLayout.setTitle("MENU1");
        appLayout.getArti().setVisible(false);


    }


    Paragraph getLabel() {
        Paragraph title = new Paragraph("Bienvenido a la aplicación de inventarios CoMiSoftware, en esta aplicación usted podrá registrar ventas, registrar compras" +
                "ver el inventario de la empresa, todos sus artículos bien organizados, buscar nunca ha sido tan fácil!." +
                "Cuenta con un sistema de reportes el cual le brindará información y detalle sobre el desempeño de su empresa, puede generar reportes semanales, mensuales e incluso" +
                "personalizados. ");
        title.setWidth(null);
        return title;
    }
    protected void excluir_rol(VaadinRequest r){
        VaadinSession.getCurrent().getSession().setAttribute("rol",VaadinRequest.getCurrent().getRemoteUser());
    }


}