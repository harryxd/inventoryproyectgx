package pe.edu.unsa.gabrielalex.spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pe.edu.unsa.gabrielalex.spring.entity.Articulos;

public interface ArticulosRepository extends JpaRepository<Articulos, String>{
}