package pe.edu.unsa.gabrielalex.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import pe.edu.unsa.gabrielalex.spring.Security.SecurityConfiguration;
import pe.edu.unsa.gabrielalex.spring.entity.Usuario;
import pe.edu.unsa.gabrielalex.spring.repository.UsuarioRepository;
import pe.edu.unsa.gabrielalex.spring.service.UsuarioService;
import pe.edu.unsa.gabrielalex.spring.view.MainView;

/**
 * The entry point of the Spring Boot application.
 */
@SpringBootApplication( scanBasePackageClasses = { SecurityConfiguration.class, MainView.class, Application.class,
        UsuarioService.class }, exclude = ErrorMvcAutoConfiguration.class)
@EnableJpaRepositories(basePackageClasses = { UsuarioRepository.class })
@EntityScan(basePackageClasses = { Usuario.class })
public class Application extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);

    }
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }

}
